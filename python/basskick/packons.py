import sys
import pluggy
from .api.benv import Benv

import logging
logger = logging.getLogger(__name__)

packon_plugin = pluggy.HookimplMarker("basskick.packon")


class _PackonSpecs(object):

    packon_spec = pluggy.HookspecMarker("basskick.packon")

    @packon_spec
    def declare(self):
        """Called at blender startup.
        plugins must return a list or Packon classes.
        """

class Packon(object):

    ALLOW_DISABLING = True

    @classmethod
    def packon_name(cls):
        return f"{cls.__module__}.{cls.__name__}"

    def __init__(self):
        super(Packon, self).__init__()
        self._enabled = False
        self.enable()

    def is_enabled(self):
        return self._enabled
        
    def enable(self):
        try:
            self.register()
        except Exception as err:
            logger.error(
                f'Error enabling Packon {self.packon_name()}: {err}'
            )
        else:
            self._enabled = True
        
    def register(self):
        raise NotImplementedError()

    def disable(self):
        if not self.ALLOW_DISABLING:
            logger.info(f'Abort disable(): not allowed on Packon {self}')
            return
        try:
            self.unregister()
        except Exception as err:
            logger.error(
                f'Error disabling Packon {self.packon_name()}: {err}'
            )
        finally:
            self._enabled = False
    
    def unregister(self):
        raise NotImplementedError()

if 0:
    #
    #   THIS WAS AN OLD DIRTY TRICK
    #   (keeping it as ref for some time...)
    class FakeOject(object):

        def __init__(self, name, *args, **kwargs):
            super(FakeOject, self).__init__()
            self._fakeobject_name = name
            self._fakeattrs = {}
        
        def _add_type(self, name):
            fake_type = type(name, (FakeOject,), {})
            fake_type.__module__ = self._fakeobject_name
            self._fakeattrs['name'] = fake_type
            return fake_type

        def __getattr__(self, name):
            if name.startswith('_'):
                return self.__getattribute__(name)
            try:
                return self._fakeattrs[name]
            except KeyError:
                if self._fakeobject_name == 'bpy.types':
                    fo = self._add_type(name)
                else:
                    fo = FakeOject(self._fakeobject_name+'.'+name)
                self._fakeattrs[name] = fo
                return fo
        
        def __setattr__(self, name, value):
            if not name.startswith('_'):
                self._fakeattrs[name] = value
            else:
                super(FakeOject, self).__setattr__(name, value)

        def __iter__(self):
            return iter(self._fakeattrs)

        def __call__(self, *args, **kwargs):
            return FakeOject(
                '{}(*{},**{})'.format(
                    self._fakeobject_name, 
                    args, kwargs
                )
            )

    def fake_bpy():
        try:
            import bpy
        except ImportError:
            #TODO: go to hell for this ? :/
            sys.modules['bpy'] = FakeOject('bpy')
            bl_ui = FakeOject('bl_ui')
            bl_ui.space_userpref._add_type('PreferencePanel')
            bl_ui.space_userpref._add_type('Panel')
            sys.modules['bl_ui'] = bl_ui
            sys.modules['bl_ui.space_userpref'] = bl_ui.space_userpref

class PackonsManager(object):

    _ = None

    @classmethod
    def get(cls):
        return cls._            

    def __init__(self, benv_path):
        super(PackonsManager, self).__init__()
        PackonsManager._ = self

        self.benv = Benv(benv_path)
        
        namespace = "basskick.packon"
        pm = pluggy.PluginManager(namespace)
        if 0:
            pm.trace.root.setwriter(print)
            undo = pm.enable_tracing()
        pm.add_hookspecs(_PackonSpecs)

        logger.info(
            f'PackonManager importing Packons from entry point "{namespace}"...'
        )
        pm.load_setuptools_entrypoints(namespace)

        logger.info(
            f'PackonManager importing factory Packons...'
        )
        from basskick.lib.packons import (
            about, 
            manager,
        )
        pm.register(about)
        pm.register(manager)

        self.hook = pm.hook

        self._packons = {}

        self._init_packons()

    def _init_packons(self):
        logger.info('PackonManager Packons declarations...')
        packon_classes = sum(self.hook.declare(), [])
        
        logger.info('PackonManager Packons Initialisation...')
        for pc in packon_classes:
            name = pc.packon_name()
            logger.info(f'    {name}')
            packon = pc()
            self._packons[name] = packon
            
    def packon_names(self):
        return self._packons.keys()
    
    def is_enabled(self, packon_name):
        return self._packons[packon_name].is_enabled()
                
    def enable(self, packon_name):
        self._packons[packon_name].enable()

    def disable(self, packon_name):
        self._packons[packon_name].disable()

    def packon_info(self, packon_name):
        """
        Returns a dict keys:
            module_name
            description
        """
        packon = self._packons[packon_name]
        return dict(
            module_name=packon.__module__, 
            description=packon.__doc__,
        )
