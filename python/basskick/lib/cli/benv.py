import click
from basskick.cli import cli_plugin_impl, session_arg, get_cli_session


@cli_plugin_impl
def define_commands(group: click.Group, in_benv: bool):
    if group.name == 'basskick' and not in_benv:
        group.add_command(benv_cmds, name='benv')

#
# Commands
#

@click.group(name='benv')
def benv_cmds():
    """
    Manage benvs (blender virtualenvs)
    """
    pass

@benv_cmds.command(name='create')
@click.argument('blender_name')
@click.argument('name')
@click.argument('location', default='.')
@click.option(
    '-f', '--force', is_flag=True, required=False,
    help='Allow update of a existing benv name.',
)
@session_arg
def create_benv(blender_name, name, location, session=None, force=False):
    '''
    Creates a benv with the given blender in the given 
    location and register it in the session.

    The blender name must be an know installation in 
    the session.

    If a benv with this name is already known, the
    --force flag must be set.
    '''
    session = get_cli_session(session)
    forget = False
    if name in session.get_benv_names():
        if force:
            click.echo(f'Overriding benv name "{name}"')
            forget = True
        else:
            click.echo(f'A benv "{name}"" already exists. Use --force to update it.')
            return
    blender_install = session.get_install(blender_name)
    blender_install.create_benv(name, location)
    if forget:
        session.forget_benv(name)
    session.add_benv(name, location)
    session.save()

@benv_cmds.command(name='list')
@session_arg
def list_benv(session=None):
    """
    Lists the benvs registered in the session.
    """
    session = get_cli_session(session)
    names = sorted(session.get_benv_names())
    if names:
        click.echo('Know benvs:')
        for name in names:
            click.echo('  '+name)
    else:
        click.echo('No benv registered.')

@benv_cmds.command(name='forget')
@click.argument('name')
@session_arg
def forget_benv(name, session=None):
    """
    Removes a benv from the session.
    """
    session = get_cli_session(session)
    session.forget_benv(name)
    session.save()