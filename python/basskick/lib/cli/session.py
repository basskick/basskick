import click
from basskick.cli import cli_plugin_impl, session_arg, get_cli_session


@cli_plugin_impl
def define_commands(group: click.Group, in_benv: bool):
    if group.name == 'basskick' and not in_benv:
        group.add_command(session_cmds, name='session')

#
# Commands
#

@click.group(name='session')
def session_cmds():
    """
    Configure and inspect session.
    """
    pass

@session_cmds.command()
@click.argument('path', required=False)
@click.option(
    '-f', '--force', is_flag=True, required=False,
    help='Allow overwrite of a existing session file.',
)
def create_session_file(path='.', force=False):
    """
    Create a default session in `path`.
    """
    existing = session.BasskickSession.find_session_file(path)
    if existing and not force:
        click.echo('Session file already exists: "{}" (use -f to overwrite)'.format(existing))
        return
    elif force:
        click.echo('Overwritting session file: "{}"'.format(existing))
    new_session = session.BasskickSession()
    new_session.save(path)
    click.echo('Session file saved: "{}"'.format(new_session.filename()))

@session_cmds.command()
@click.argument('path', required=True)
@click.argument('version', required=True)
@click.argument('name', required=False)
@click.option(
    '-u', '--update', is_flag=True, required=False,
    help='Allow update if an installation with this name already exists',
)
@session_arg
def add_install(path, version, name=None, session=None, update=False):
    '''
    Registers a blender installation.
    The `version` argument must match the version folder inside `path`
    '''
    if name is None:
        name = version
    session = get_cli_session(session)
    try:
        install_name = session.add_install(name, version, path, allow_update=update)
    except Exception as err:
        click.echo("Error adding blender install: {}".format(err))
    else:
        click.echo("Added blender install: '{}'".format(name))
    session.save()

@session_cmds.command()
@session_arg
def list_installs(session=None):
    '''
    List blender installations
    '''
    session = get_cli_session(session)
    installs = session.get_install_names()
    if not installs:
        click.echo('No installation registered.')
        return
    click.echo('Registered Blender Installations:')
    for name in installs:
        version = session.get_install_version(name)
        path = session.get_install_path(name)
        click.echo(
            '   {}: {} ({})'.format(
                name, version, path
            )
        )

@session_cmds.command(name='forget-install')
@click.argument('name')
@session_arg
def forget_install(name, session=None):
    """
    Removes a blender installation from the session.
    """
    session = get_cli_session(session)
    session.forget_install(name)
    session.save()

