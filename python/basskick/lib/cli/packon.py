import click
from basskick.cli import cli_plugin_impl, session_arg, get_cli_session


@cli_plugin_impl
def define_commands(group: click.Group, in_benv: bool):
    if group.name == 'basskick' and not in_benv:
        group.add_command(packon_cmds, name='packon')

#
# Commands
#

@click.group(name='packon')
def packon_cmds():
    """
    Manage packons (packaged blender addons)
    """
    pass

@packon_cmds.command(name='create')
@click.argument('name')
@click.option('-b', '--benv', required=False)
@session_arg
def create_packon(name, benv=None, session=None):
    '''
    Create a new packon in the current or specified benv.
    '''
    session = get_cli_session(session)
    benv = benv or None
    packon.create(session, name, benv) #!

@packon_cmds.command(name='install')
@click.argument('names', nargs=-1)
@click.option('-b', '--benv', required=False)
@click.option('-U', '--upgrade', is_flag=True, help='Upgrade the specified packons.')
@click.option('-i', '--index-url', required=False, help='custom index url')
@session_arg
def install_packons(names, benv=None, upgrade=False, index_url=None, session=None):
    '''
    Install packons in the current or specified benv.
    '''
    session = get_cli_session(session)
    packon.install(session, names, benv, upgrade, index_url) #!

@packon_cmds.command(name='uninstall')
@click.argument('names', nargs=-1)
@click.option('-b', '--benv', required=False)
@session_arg
def uninstall_packons(names, benv=None, session=None):
    '''
    Uninstall packons in the current or specified benv.
    Note that dependencies will not be uninstalled.
    '''
    session = get_cli_session(session)
    packon.uninstall(session, names, benv) #!

@packon_cmds.command(name='list')
@click.option('-b', '--benv', required=False)
@click.option('-o', '--outdated', default=True, help='List outdated packons')
@click.option('-u', '--uptodate', default=True, help='List uptodate packons')
@click.option('-e', '--editable', default=True, help='List editable packons')
@session_arg
def list_packons(benv=None, outdated=True, uptodate=True, editable=True, session=None):
    '''
    Lists installed packons.
    '''
    session = get_cli_session(session)
    packon.list(session, benv, outdated, uptodate, editable) #!


@packon_cmds.command(name='show')
@click.argument('names', nargs=-1)
@click.option('-b', '--benv', required=False)
@session_arg
def show_packons(names, benv=None, session=None):
    '''
    Show information about installed packons in the current or specified benv.
    '''
    session = get_cli_session(session)
    packon.show(session, names, benv) #!

@packon_cmds.command()
@click.argument('addon_path')
@click.option('-b', '--benv_name', required=True)
@session_arg
def install_addon(addon_path, benv_name=None, session=None):
    """
    Converts and install a classic blender addon in the
    specifier benv.

    Addon path may be the path of a module or package addon,
    or the path of an addon zip file (ending with .zip).
    """
    from basskick.api.addon import install_addon
    session = get_cli_session(session)
    benv_path = session.get_benv_path(benv_name)
    install_addon(addon_path, benv_path)