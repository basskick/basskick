import click
from basskick.cli import cli_plugin_impl, session_arg, get_cli_session
from basskick.packons import PackonsManager


@cli_plugin_impl
def define_commands(group: click.Group, in_benv: bool):
    if group.name == 'basskick':
        if in_benv:
            group.add_command(current_blender)
        else:
            group.add_command(benv_blender)
        

#
# Commands
#

## In Benv Commands

@click.command('blender')
def current_blender():
    """
    Run blender current benv's blender.
    """
    from basskick.api.benv import Benv
    benv = Benv.current_benv()
    benv.run_blender()


## Outside Benv Commands

@click.command('blender')
@click.option('-b', '--benv_name', required=True)
@session_arg
def benv_blender(benv_name=None, session=None):
    """
    Run blender in specified benv.
    """
    session = get_cli_session(session)
    benv = session.get_benv(benv_name)
    benv.run_blender()
