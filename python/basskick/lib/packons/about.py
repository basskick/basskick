from basskick.packons import packon_plugin, Packon, PackonsManager

import sys
import bpy


class Basskick_OT_ShowPref(bpy.types.Operator):
    bl_idname = 'basskick.show_prefs'
    bl_label = 'Show Prefs'
    # bl_options = {"REGISTER", "UNDO"}
 
    def execute(self, context):
        # Ensure no addon is displayed to make room for Packons:
        context.window_manager.addon_search =  '!! BASSKICK ONLY !!' 

        # Select the Add-ons page in the preference panel:
        context.preferences.active_section = 'ADDONS'

        # Show the preference panel:
        bpy.ops.screen.userpref_show('INVOKE_DEFAULT')
        return {"FINISHED"}

class TOPBAR_MT_app_basskick_about(bpy.types.Menu):
    bl_label = "About Basskick"

    def draw(self, _context):
        layout = self.layout

        layout.operator_context = 'INVOKE_DEFAULT'
        layout.operator("basskick.show_prefs")

        layout.separator()

        layout.label(
            text='Benv: '+PackonsManager.get().benv.prompt()
        )
        layout.label(text='Version: LOL')

        layout.separator()

        layout.operator(
            "wm.url_open", text="Home", icon='URL',
        ).url = "https://www.basskick.org"

def basskick_about_draw(self, context):
    layout = self.layout
    layout.separator()
    layout.menu("TOPBAR_MT_app_basskick_about")

#
#   PACKON
#

class BasskickAbout(Packon):

    def register(self):
        bpy.utils.register_class(TOPBAR_MT_app_basskick_about)
        bpy.utils.register_class(Basskick_OT_ShowPref)
        bpy.types.TOPBAR_MT_app.append(basskick_about_draw)

    def unregister(self):
        bpy.utils.unregister_class(TOPBAR_MT_app_basskick_about)
        bpy.utils.unregister_class(Basskick_OT_ShowPref)
        bpy.types.TOPBAR_MT_app.remove(basskick_about_draw)

@packon_plugin
def declare():
    return [BasskickAbout]

