from basskick.packons import packon_plugin, Packon, PackonsManager

import bpy


PACKON_EXPANDED = set()


class USERPREF_PT_system_basskick_packon_expand(bpy.types.Operator):
    """Display information and preferences for this packon"""
    bl_idname = "basskick.pref_addon_expand"
    bl_label = ""
    bl_options = {'INTERNAL'}

    packon: bpy.props.StringProperty(
        name="Packon",
        description="Name of the packon to expand",
    )

    def execute(self, _context):
        if self.packon in PACKON_EXPANDED:
            PACKON_EXPANDED.remove(self.packon)
        else:
            PACKON_EXPANDED.add(self.packon)

        return {'FINISHED'}

class USERPREF_PT_system_basskick_packon_enable(bpy.types.Operator):
    """Enable this packon"""
    bl_idname = "basskick.packon_enable"
    bl_label = ""
    bl_options = {'INTERNAL'}

    packon: bpy.props.StringProperty(
        name="Packon",
        description="Name of the packon to enable",
    )

    def execute(self, _context):
        PackonsManager.get().enable(self.packon)
        return {'FINISHED'}

class USERPREF_PT_system_basskick_packon_disable(bpy.types.Operator):
    """Disable this packon (if it allows it)"""
    bl_idname = "basskick.packon_disable"
    bl_label = ""
    bl_options = {'INTERNAL'}

    packon: bpy.props.StringProperty(
        name="Packon",
        description="Name of the packon to disable",
    )

    def execute(self, _context):
        PackonsManager.get().disable(self.packon)
        return {'FINISHED'}

#
#   PANEL
#

class USERPREF_PT_system_basskick(bpy.types.Panel):
    bl_space_type = 'PREFERENCES'
    bl_region_type = 'WINDOW'
    bl_context = "addons"
    bl_label = "Basskick Packons"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Yeah \\o/")

        manager = PackonsManager.get()
        for packon_name in sorted(manager.packon_names()):
            
            enabled = manager.is_enabled(packon_name)
            expanded = packon_name in PACKON_EXPANDED            

            box = layout.box()
            colsub = box.column()
            row = colsub.row(align=True)

            row.operator(
                "basskick.pref_addon_expand",
                icon='DISCLOSURE_TRI_DOWN' if expanded else 'DISCLOSURE_TRI_RIGHT',
                emboss=False,
            ).packon = packon_name

            row.operator(
                "basskick.packon_disable" if enabled else "basskick.packon_enable",
                icon='CHECKBOX_HLT' if enabled else 'CHECKBOX_DEHLT', text="",
                emboss=False,
            ).packon = packon_name

            sub = row.row()
            sub.active = enabled
            sub.label(text=packon_name)

            if expanded:
                info = manager.packon_info(packon_name)

                split = colsub.row().split(factor=0.25)
                split.label(text="Module: ")
                split.label(text=info["module_name"], translate=False)

                split = colsub.row().split(factor=0.25)
                split.label(text="Description:")
                col = split.column()
                for line in (info["description"] or 'No doc :/').split('\n'):
                    col.label(text=line)


#
#   PACKON
#

class BasskickPackonManager(Packon):
    """
    Show basskick and current benv info in "Preferences > Add-ons"
    (Cannot be disabled)
    """

    ALLOW_DISABLING = False

    BL_CLASSES = (
        USERPREF_PT_system_basskick_packon_expand,
        USERPREF_PT_system_basskick_packon_enable,
        USERPREF_PT_system_basskick_packon_disable,
        USERPREF_PT_system_basskick,
    )

    def register(self):
        for c in self.BL_CLASSES:
            bpy.utils.register_class(c)

    def disable(self):
        # Disallow the packon disabling
        pass

@packon_plugin
def declare():
    return [BasskickPackonManager]

