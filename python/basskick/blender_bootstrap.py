import os
import site

#! This file must not import non std lib packages
# (it needs to be execfile-able within blender)


def bootstrap_benv():
    """
    This is executed at blender startup to make the benv site
    available inside blender and create the basskick.packons.PackonManager()

    It uses BASSKICK_BENV_PATH env var to find the site dir.
    """
    benv_path = os.environ['BASSKICK_BENV_PATH']
    benv_site_dir = os.path.join(benv_path, 'Lib', 'site-packages')
    print('Basskick Bootstrap adding benv site:', benv_site_dir)
    site.addsitedir(benv_site_dir)

    import basskick
    from basskick.packons import PackonsManager
    mng = PackonsManager(benv_path)

if __name__ == '__main__':
    bootstrap_benv()