__path__ = __import__('pkgutil').extend_path(__path__, __name__)

import logging
logging.basicConfig(
    format='[%(name)s::%(levelname)s] %(message)s',
    level=logging.INFO,
)

logger = logging.getLogger(__name__)
logger.info('Let the BASS Kick !')
