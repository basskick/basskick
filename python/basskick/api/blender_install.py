"""

Notes:
    blender.exe does not have a -m option like python.
    the official way to mimic -m is the runpy module.
    but blender does not ship with runpy /:(
    So we can't do thing like:
        args = [
            # run blender in background (python mode)
            '-b', 
            # change sys.argv to control pip:
            '--python-expr', 'import sys; sys.argv = ["install", "virtualenv"]',
            # trigger pip the same way as 'python -m pip':
            '--python-expr', 'runpy.run_module("pip")',
        ]



"""
import os
import subprocess

from .benv import Benv
import logging
logger = logging.getLogger(__name__)


class BlenderInstall(object):

    def __init__(self, name, version, path):
        super(BlenderInstall, self).__init__()
        self.name = name
        self.version = version
        self.path = path
    
    def blender_exe(self):
        #TODO: support non-windows platform
        return os.path.join(
            self.path, 'blender.exe'
        )

    def python_exe(self):
        #TODO: support non-windows platform
        return os.path.join(
            self.path, self.version, 'python', 'bin', 'python.exe'
        )

    def _run(self, cmd, wait=True):
        """
        Args:
            cmd : list of strings.
            wait: returns after subprocess ended.

        Returns:
            False if there was an error running the cmd
            True otherwise (does not mean cmd suceeded !)
        """
        logger.info(
            'Running command: {}'.format(
                " ".join(cmd)
            )
        )
        try:
            popen = subprocess.Popen(cmd)
        except Exception:
            logger.critical(
                msg="command was {}".format(
                    " ".join(cmd),
                ),
                exc_info=True,
            )
            return False

        if wait:
            popen.wait()
        return True

    if 0:
        # not used, keept as reference
        def _run_blender_m(self, module_name, *args):
            """
            Run python command from blender as if it supported python's -m option:
                blender -m module_name arg arg arg
            """
            all_args = (
                # run blender in background (python mode)
                '-b', 
                # change sys.argv to pass arguments:
                '--python-expr', '"import sys; sys.argv = {!r}; import runpy; runpy.run_module(\'{}\')"'.format(
                    list(args), module_name
                ),
                # # trigger pip the same way as 'python -m pip':
                # '--python-expr', 'import runpy; runpy.run_module("{}")'.format(module_name),
            )
            cmd = (self.blender_exe(),)+all_args
            return self._run(cmd, wait=True)

    def _run_blender(self, *args):
        cmd = (self.blender_exe(),)+args
        return self._run(cmd, wait=True)

    def _run_python(self, *args):
        cmd = (self.python_exe(),)+args
        return self._run(cmd, wait=True)

    def ensure_pip(self):
        args = [
            '-m', 'ensurepip'
        ]
        return self._run_python(*args)

    def ensure_virtualenv(self):
        if not self.ensure_pip():
            return False
        args = [
            '-m', 'pip', 'install', 'virtualenv'
        ]
        return self._run_python(*args)

    def create_benv(self, name, location):
        logger.info(
            'Creating benv "{}" with blender {} in {}'.format(
                name, self.blender_exe(), location
            )
        )
        if not self.ensure_virtualenv():
            return

        dest = os.path.join(location, name)

        if 0:
            # This one did not trigger venv creation, I guess there's something no picking up sys.argv:
            self._run_blender_m('virtualenv', '--prompt', name, dest, '--python', self.python_exe())

        if 0:
            # This did not work with 2.80, the virtualenv contains blender but not python. I dont
            # know if it's a problem really but a venv without python sounds bad. Adding another
            # virtualenv with blender's python fixes it, but this is too hacky for me.
            #
            # Also, this did not work with blender-2.83-f9cca128869f-windows64: missing dlls
            # (the ones in $INSTALL/blender.crt). I guess nightly build can have a special structure...
            #
            self._run_blender(
                '-b', '--python-expr', "import virtualenv.__main__ as v;v.run({})".format(
                    ['--prompt', name, dest, '--python', self.blender_exe(), '--clear', '--system-site-packages']
                )
            )

        self._run_python(
            '-m', 'virtualenv', '--prompt', f"{{{name}}} ", dest, 
            # Not sure about this one, let's not until it fails:
            # '--system-site-packages',
        )

        benv = Benv(dest)
        benv.install('basskick')

        return benv