'''




'''


import logging
logger = logging.getLogger(__name__)

def create(settings, name, benv=None):
    logger.info(settings)
    logger.info(
        'Creating Packons {} in {}'.format(
            name,
            benv and 'benv {}'.format(benv) or 'current benv',
        )
    )

def install(settings, names, benv=None, upgrade=False, index_url=None):
    logger.info(settings)
    logger.info(
        '{} Packons "{}" in {} from {}'.format(
            upgrade and 'Upgrading' or 'Installing',
            ', '.join(names),
            benv and 'benv {}'.format(benv) or 'current benv',
            index_url and 'index {}'.format(index_url) or 'default index',
        )
    )

def uninstall(settings, names, benv=None):
    logger.info(settings)
    logger.info(
        'Uninstalling Packons {} from {}'.format(
            names,
            benv and 'benv {}'.format(benv) or 'current benv',
        )
    )

def list(settings, benv, outdated, uptodate, editable):
    logger.info(settings)
    logger.info(
        'Listing Packons ({}) in {}'.format(
            'outdated={}, uptodate={}, editable={}'.format(
                outdated, uptodate, editable
            ),
            benv and 'benv {}'.format(benv) or 'current benv',
        )
    )

def show(settings, names, benv):
    logger.info(settings)
    logger.info(
        'Informations for {} packon(s) in {}'.format(
            names and ', '.join(names) or 'all',
            benv and 'benv {}'.format(benv) or 'current benv',
        )
    )
