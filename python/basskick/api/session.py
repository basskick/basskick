"""
TODO: have a base class for Session to inherite as this file session and custom session. 

"""

import sys
import os
import json
import pprint

from . import blender_install
from . import benv

import logging
logger = logging.getLogger(__name__)


class BasskickSession(object):

    _SESSION_FILE_BASENAME = '.basskick'

    _DEFAULTS_SETTINGS = {

    }

    @classmethod
    def is_in_benv(cls):
        """
        Returns True if the current environment 
        is a benv.
        """
        return benv.Benv.is_in_benv()
    
    @classmethod
    def current_benv(cls):
        """
        Returns a Benv() for the current benv.
        If the current environment is not a benv,
        a benv.IsNotABenv exception is raised.
        """
        return benv.Benv.current_benv()
        
    @classmethod
    def _find_file(cls, path, filename):
        path = os.path.abspath(path)
        here = os.path.join(path, filename)
        if os.path.isfile(here):
            return here
        dirname = os.path.dirname(path)
        if not dirname or dirname == path:
            return None        
        return cls._find_file(dirname, filename)

    @classmethod
    def find_session_file(cls, path):
        return cls._find_file(path, cls._SESSION_FILE_BASENAME)

    @classmethod
    def from_path(cls, path):
        filename = cls.find_session_file(path)
        if filename is not None:
            return cls.from_file(filename)
        return cls()

    @classmethod
    def from_file(cls, filename):
        try:
            with open(filename, 'r') as fp:
                session_data = json.load(fp)
        except Exception as err:
            raise ValueError(
                'Error reading session file {}: {}'.format(
                    filename, err
                )
            )
        logger.info('{} loaded from {}'.format(cls.__name__, filename))
        session = cls()
        session._update_data(session_data)
        session._filename = filename
        return session

    def __init__(self):
        super(BasskickSession, self).__init__()
        self._filename = None
        self._data = {}
        self._update_data(self._DEFAULTS_SETTINGS)
    
    def pformat(self):
        return "{}.{}@{}:{}".format(
            __name__,
            self.__class__.__name__,
            self.filename() or "<no filename>",
            pprint.pformat(self._data),
        )

    def __str__(self):
        return self.pformat()

    def filename(self):
        return self._filename
    
    def save(self, path=None):
        """
        Stores the session in the `path` folder so that
        it can be discoverable from sub-folders.

        If path is None, filename() value is used and
        must be set.
        """
        if path:
            self._filename = os.path.join(
                os.path.abspath(path),
                self._SESSION_FILE_BASENAME
            )
        elif self.filename() is None:
            raise Exception(
                'Could not save session: no filename set.'
            )
        with open(self.filename(), 'w') as fp:
            json.dump(self._data, fp)
        logger.debug('{} {} saved to disk'.format(
            self.__class__.__name__,
            self,
        ))

    def _update_data(self, data):
        self._data.update(data)

    def _get(self, name, *default):
        return self._data.get(name, *default)
    
    def _get_typed(self, name, type_factory):
        try:
            return self._data[name]
        except KeyError:
            v = type_factory()
            self._data[name] = v
            return v
    
    def _get_dict(self, name):
        return self._get_typed(name, dict)

    def _get_list(self, name):
        return self._get_typed(name, list)

    def _set(self, name, value):
        logger.info('{!r} set to {!r}'.format(name, value))
        self._data[name] = value

    def add_install(self, name, version, path, allow_update=False):
        """
        Registers a Blender installation in the session.

        The `version` arg must be the name of the `path` subfolder
        containing the "datafiles", "python", and "scripts" folders.
        """
        installs = self._get_dict('installs')
        try:
            existing = installs[name]
        except KeyError:
            pass
        else:
            if not allow_update:
                raise ValueError(
                    'An installation named "{}" is already registered as "{}"'.format(
                        name, existing
                    )
                )
        installs[name] = dict(path=path, version=version)
        return name
        
    def forget_install(self, name):
        installs = self._get_dict('installs')
        try:
            installs.pop(name)
        except:
            raise ValueError(
                'No "{}" installation registered.'.format(
                    name,
                )
            )

    def get_install_path(self, name):
        installs = self._get_dict('installs')
        try:
            return installs[name]['path']
        except KeyError:
            raise ValueError(
                'No installs named "{}" registered'.format(
                    name
                )
            )

    def get_install_version(self, name):
        installs = self._get_dict('installs')
        try:
            return installs[name]['version']
        except KeyError:
            raise ValueError(
                'No installs named "{}" registered'.format(
                    name
                )
            )

    def get_install_names(self):
        installs = self._get_dict('installs')
        return installs.keys()

    def get_install(self, name):
        installs = self._get_dict('installs')
        try:
            install_config = installs[name]
        except KeyError:
            raise ValueError(f'No such install "{name}"')
        return blender_install.BlenderInstall(
            name, install_config['version'], install_config['path']
        )

    def add_benv(self, name, location):
        benvs = self._get_dict('benvs')
        try:
            existing = benvs[name]
        except KeyError:
            pass
        else:
            raise ValueError(
                'A benv named "{}" is already registered as "{}"'.format(
                    name, existing
                )
            )

        path = os.path.join(
            os.path.abspath(location),
            name
        )
        benvs[name] = path

    def forget_benv(self, name):
        benvs = self._get_dict('benvs')
        try:
            benvs.pop(name)
        except:
            raise ValueError(
                'No "{}" benv registered.'.format(
                    name,
                )
            )

    def get_benv_path(self, name):
        benvs = self._get_dict('benvs')
        try:
            return benvs[name]
        except KeyError:
            raise ValueError(
                'No benv named "{}" registered'.format(
                    name
                )
            )

    def get_benv_names(self):
        benvs = self._get_dict('benvs')
        return benvs.keys()
    
    def get_benv(self, name):
        path = self.get_benv_path(name)
        return benv.Benv(path)