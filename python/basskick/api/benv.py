'''




'''
import sys
import os
import subprocess
import configparser

from .. import blender_bootstrap

import logging
logger = logging.getLogger(__name__)

class NotAVenvError(Exception):
    pass

class NotABenvError(NotAVenvError):
    pass

class Benv(object):
    """
    Operates on a blender virtualenv
    """

    @classmethod
    def get_venv_config(cls, venv_path):
        config = configparser.ConfigParser()
        config_path = os.path.join(venv_path, "pyvenv.cfg")
        if not os.path.exists(config_path):
            raise NotAVenvError(
                f"Bad venv path: could not find config file '{config_path}'."
            )

        with open(config_path, 'r') as fp:
            content = fp.read()
        config.read_string('[ALL]\n'+content)
        return config['ALL']

    @classmethod
    def get_benv_blender(cls, benv_path):
        #TODO: support non-windows platforms
        home = cls.get_venv_config(benv_path)['home']
        blender_exe = os.path.normpath(
            os.path.join(home, "..", "..", "..", "blender.exe")
        )
        if not os.path.exists(blender_exe):
            raise NotABenvError(
                f"Blender not found at {blender_exe} :/\n"
                f"(Please check that {sys.prefix} is a valid benv.)"
            )
        return blender_exe

    @classmethod
    def is_in_benv(cls):
        if not hasattr(sys, 'base_prefix') or sys.prefix == sys.base_prefix:
            # This means we are not even in a virtualenv
            return False
        try:
            cls.get_benv_blender(sys.prefix)
        except (NotABenvError, NotAVenvError):
            return False
        return True

    @classmethod
    def current_benv(cls):
        benv_path = sys.prefix
        # Assert this is a benv path:
        cls.get_benv_blender(benv_path)
        return cls(benv_path)

    def __init__(self, path):
        super(Benv, self).__init__()
        self.path = path
        self._config = None

    def venv_config(self, key):
        if self._config is None:
            self._config = self.get_venv_config(self.path)
        return self._config[key]

    def prompt(self):
        return self.venv_config('prompt')

    def find_blender(self):
        """
        Returns the benv's blender executable
        """
        return self.get_benv_blender(self.path)

    def find_site_packages(self):
        #TODO: support non-windows platforms
        return os.path.join(
            self.path, 'Lib', 'site-packages'

        )

    def find_script(self, script_name):
        #TODO: support non-windows platforms
        return os.path.join(
            self.path,
            'Scripts', 
            script_name+'.exe'
        )

    def _run(self, cmd, wait=True, **extra_env):
        """
        Args:
            cmd : list of strings.
            wait: returns after subprocess ended.
            **extra_env: env var to add for subprocess.

        Returns:
            False if there was an error running the cmd
            True otherwise (does not mean cmd suceeded !)
        """
        logger.info(
            'Running command: {}'.format(
                " ".join(cmd)
            )
        )
        kwargs = {}
        if extra_env:
            kwargs['env'] = os.environ.copy()
            kwargs['env'].update(extra_env)
        try:
            popen = subprocess.Popen(cmd, **kwargs)
        except Exception:
            logger.critical(
                msg="command was {}".format(
                    " ".join(cmd),
                ),
                exc_info=True,
            )
            return False

        if wait:
            popen.wait()
        return True

    def run(self, script_name, *args):
        script = self.find_script(script_name)
        cmd = (script,)+args
        return self._run(cmd, wait=True)

    def run_blender(self, *cmd_args):
        exe = self.find_blender()
        
        bootstrap_file = os.path.abspath(
            blender_bootstrap.__file__
        ).replace(' ', '\\ ')
        cmd = (exe, "--python", bootstrap_file,)+cmd_args
        return self._run(
            cmd, wait=False, 
            BASSKICK_BENV_PATH=self.path, 
        )

    def install(self, *module_names):
        return self.run('pip', 'install', '--upgrade', *module_names)

# def create(name, location):
#     try:
#         import ensurepip
#     except ImportError:
#         raise Exception(
#             'You cannot use this python to create a benv: ensurepip not found.'
#         )

#     logger.info('Creating benv {} in {}'.format(name, location))

#     #FIXME: this should take a blender version (the benv is created by blender)

# def run(benv_path, command_name, command_args):
#     """
#     Runs the given command in the benv at benv_path
#     """
#     raise NotImplementedError()
#     # 1) find the blender version (download if needed)
#     # 2) run it with args: -b --python-expr "import pip.__main__ as m; import sys; sys.argv=['install', 'venv']; exec(open(m.__file__).read())"
#     #   => actually, try and use runpy.run_module() instead.
