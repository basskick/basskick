"""


"""
import os
import sys
import re
import setuptools
import tempfile
import zipfile
import shutil

from .benv import Benv

import logging
logger = logging.getLogger(__name__)

class AddonConverter(object):

    def __init__(self, addon_path):
        super(AddonConverter, self).__init__()
        self.addon_path = addon_path
        if not os.path.exists(self.addon_path):
            raise ValueError(
                f'Addon path does not exists: {addon_path}'
            )

        self.main_file = addon_path
        self.is_module = os.path.isfile(addon_path)

        self.is_package = os.path.isdir(addon_path)
        if self.is_package:
            self.main_file = os.path.join(self.addon_path, '__init__.py')
        
        self._bl_info = None
        self.dist_path = None

    @property
    def bl_info(self):
        if self._bl_info is None:
            self._bl_info = self._read_bl_infos()
        return self._bl_info

    @property
    def addon_name(self):
        if self.is_module:
            addon_name = os.path.basename(self.addon_path).split('.', 1)[0]
        else:
            addon_name = os.path.basename(self.addon_path)
        return addon_name

    def _read_bl_infos(self):
        with open(self.main_file) as fh:
            script = fh.read()
        match = re.search(r'bl_info\s*=\s*{', script)
        if match is None:
            return dict(
                description=f"{self.addon_name} addon converted to packon (no description found)",
                author="???",
                blender=(2,83,0),
            )
            # raise Exception(
            #     'Could not find "bl_info = " in {}'.format(
            #         filepath
            #     )
            # )

        start = match.span()[0]
        end = None
        i = match.span()[-1]
        eot = len(script)
        c = 1
        while i < eot:
            char = script[i]
            if char == '{':
                c += 1
            elif char == '}':
                c -= 1
            i += 1
            if c == 0:
                end = i
                break
        if end is None:
            raise Exception(
                'Could not extract bl_info from {}'.format(
                    filepath
                )
            )
        bl_info_script = script[start:end]
        namespace = {}
        exec(bl_info_script, namespace, namespace)
        bl_info = namespace.get('bl_info')
        if bl_info is None:
            raise ValueError(
                'Error getting bl_info in {}'.format(
                    filepath
                )
            )

        return bl_info

    def get_project_urls(self):
        bl_info = self.bl_info

        # Defaults:
        project_urls = dict(
            # Documentation='https://www.blender.org',
            # Funding='https://www.blender.org',
            # Tracker=None,
            # Source=None,
            Greetings='http://www.basskick.org',
        )

        # Translate values from bl_info to project_urls
        translate = dict(
            wiki_url='Documentation',
            tracker_url='Tracker',
        )
        for bl_name, setup_name in translate.items():
            try:
                url = bl_info[bl_name]
            except KeyError:
                pass
            else:
                project_urls[setup_name] = url

        return project_urls

    def get_classifiers(self, license='LGPLv3+'):
        '''
        Supported licenses are:
            GPLv3+
            LGPLv3+
            MIT
        '''
        bl_info = self.bl_info

        classifiers = [
            # 'Development Status :: 1 - Planning',
            # 'Development Status :: 2 - Pre-Alpha',
            # 'Development Status :: 3 - Alpha',
            # 'Development Status :: 4 - Beta',
            # 'Development Status :: 5 - Production/Stable',
            # 'Development Status :: 6 - Mature',
            # 'Development Status :: 7 - Inactive',

            # 'Topic :: System :: Shells',
            'Topic :: Artistic Software',
            'Framework :: basskick',
            'Environment :: Plugins',

            # 'Intended Audience :: Developers',
            'Intended Audience :: End Users/Desktop',

            'Operating System :: OS Independent',

            # 'Programming Language :: Python :: 3.7',
        ]

        # do we really need the license classifier ? :/
        lic_to_clsfr = {
            "GPLv3+": 'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            "LGPLv3+": 'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
            "MIT": "License :: OSI Approved :: MIT License",
        }
        try:
            classifiers.append(lic_to_clsfr[license])
        except KeyError:
            raise ValueError(
                f"Unsupported license {license}, please use LGPLv3+."
            )

        return classifiers

    def get_python_requires(self):
        bl_info = self.bl_info

        # TODO: This table should have been generated by data here:
        # https://svn.blender.org/svnroot/bf-blender/tags/
        # (looking at sub path lib/windows]/python/lib/ content)
        blender_main_to_py = {
            (2, 79): (3, 4),
            (2, 80): (3, 7),
            (2, 81): (3, 7),
            (2, 82): (3, 7),
            (2, 83): (3, 7),
        }
        blender_main = bl_info['blender'][:2]
        try:
            py = blender_main_to_py[blender_main]
        except KeyError:
            raise ValueError(
                'Blender version {}.{} not supported'.format(
                    *blender_main
                )
            )
        return '>={}.{}'.format(*py)

    def get_install_requires(self):
        return ['basskick']

    def patch_addon_code(self, addon_file):
        with open(addon_file, 'r') as fp:
            content = fp.read()

        patched_flag = "-=* Converted To Packon By Basskick *=-"
        if patched_flag in content:
            return
        packon_type_name = self.addon_name.title()
        patch_lines = [
            '',
            '#',
            f'# {patched_flag}',
            '#', 
            '',
            'from basskick.packons import Packon, packon_plugin',
            '',
            f'class {packon_type_name}(Packon):',
            '',
            '    def register(self):',
            '        register()',
            '',
            '    def unregister(self):',
            '       unregister()',
            '',
            '@packon_plugin',
            'def declare():',
            f'    return [{packon_type_name}]',
            '',
        ]
        content += '\n'.join(patch_lines)
        with open(addon_file, 'w') as fp:
            fp.write(content)

    def create_packon(self, output_path, license='GPLv3+', postfix=0):
        '''
        Supported licenses are:
            GPLv3+
            LGPLv3+
            MIT
        '''
        addon_name = self.addon_name
        package_name = 'blpackon-'+addon_name.replace('_', '-')
        repo_dir = os.path.join(output_path, package_name)
        if os.path.exists(repo_dir):
            raise ValueError(f'Package {repo_dir} already exists.')
        os.mkdir(repo_dir)

        src_dir = os.path.join(repo_dir, 'src')
        os.mkdir(src_dir)
        package_dir = os.path.join(src_dir, addon_name)
        init = os.path.join(package_dir, '__init__.py')
        if self.is_package:
            shutil.copytree(self.addon_path, src_dir)
        else:
            os.mkdir(package_dir)
            shutil.copy(self.addon_path, init)
        self.patch_addon_code(init)

        readme = os.path.join(repo_dir, 'README.md')
        readme_lines = [
            '# '+addon_name,
            '',
            'Blender Addon converted to Packon by Basskick.'
            ''
        ]
        with open(readme, 'w') as fp:
            fp.write('\n'.join(readme_lines))

        bl_info = self.bl_info

        postfix_str = postfix and '.post{}'.format(postfix) or ''
        addon_version = bl_info.get('version', (0,0,1)) 
        version = '.'.join([str(i) for i in addon_version])+postfix_str

        description = bl_info['description']
        long_description = '\n'.join(readme_lines)
        project_urls = self.get_project_urls()
        author=bl_info['author']
        classifiers = self.get_classifiers(license)
        keywords = 'b3d blender addon basskick '+bl_info.get('category', '').replace(' ', '_')
        install_requires = self.get_install_requires()
        python_requires = self.get_python_requires()

        setup = os.path.join(repo_dir, 'setup.py')
        setup_lines = [
            'from setuptools import setup'
            '',
            'setup(',
            f'    name={package_name!r},',
            f'    version={version!r},',
            f'    description={description!r},',
            f'    long_description={long_description!r},',
            f'    project_urls={project_urls!r},',
            f'    author={author!r},',
            f'    # author_email=???,',
            f'    license={license!r},',
            f'    classifiers={classifiers!r},',
            f'    keywords={keywords!r},',
            f'    install_requires={install_requires!r},',
            f'    python_requires={python_requires!r},',
            f'    packages=[{addon_name!r}],',
            f'    package_dir={{"": "src"}},',
            f'    package_data={{"":["*.*"]}},',
            '    entry_points={',
            f'       "basskick.packon": ["{addon_name}={addon_name}"],',
            '   },',
            ')',
        ]
        with open(setup, 'w') as fp:
            fp.write('\n'.join(setup_lines))

        return repo_dir

    # def build_dist(self, license='GPLv3+', postfix=0):
    #     '''
    #     Supported licenses are:
    #         GPLv3+
    #         LGPLv3+
    #         MIT
    #     '''
    #     # packon_path = self.addon_path
    #     # parent_path = os.path.dirname(packon_path)
    #     # init = os.path.join(packon_path, '__init__.py')
    #     bl_info = self.bl_info

    #     addon_name = self.addon_name
    #     package_name = 'basskick-'+addon_name
    #     package_dir = os.path.dirname(self.addon_path)

    #     postfix_str = postfix and '.post{}'.format(postfix) or ''
    #     addon_version = bl_info.get('version', (0,0,1)) 
    #     version = '.'.join([str(i) for i in addon_version])+postfix_str

    #     logger.info(
    #         'Building Packon:\n'
    #         f'  addon_name = {addon_name}'
    #         f'  package_name = {package_name}'
    #         f'  package_dir = {package_dir}'
    #     )
    #     pwd = os.path.abspath('.')
    #     argv = sys.argv[:]
    #     try:
    #         os.chdir(package_dir)
    #         sys.argv[:] = [
    #             '-v',
    #             '--no-user-cfg',
    #             'bdist_wheel',
    #             '--universal',
    #         ]
    #         ret = setuptools.setup(
    #             name=package_name,
    #             version=version,
    #             description=bl_info['description'],
    #             long_description='Blender Addonn "{}":\n{}'.format(
    #                 bl_info['name'],
    #                 bl_info['description'],
    #             ),
    #             project_urls=self.get_project_urls(),
    #             author=bl_info['author'],
    #             #author_email=bl_info.get('author_email'),
    #             license=license,
    #             classifiers=self.get_classifiers(license),
    #             keywords='b3d blender addon basskick '+bl_info.get('category').replace(' ', '_'),
    #             install_requires=self.get_install_requires(),
    #             python_requires=self.get_python_requires(),
    #             packages=[addon_name],
    #             package_data={'':['*.*']},
    #             entry_points={
    #                 'basskick.packon': [f'{addon_name}={addon_name}'],
    #             },
    #         )
    #     finally:
    #         os.chdir(pwd)
    #         sys.argv[:] = argv

    #     dist_path = ret.dist_files[0][-1]
    #     self.dist_path = os.path.join(parent_path, dist_path)
    #     return self.dist_path

    # def upload_dist(dist_filename, test_index=False):
    #     from twine.commands.upload import upload
    #     from twine.settings import Settings

    #     #TODO: support custom index (and other twine args...)
    #     if 1 or test_index:
    #         repository_url = 'https://test.pypi.org/legacy/'

    #     settings = Settings(
    #         repository_url=repository_url,
    #     )
    #     upload(
    #         settings,
    #         [dist_filename],
    #     )

def convert_addon(addon_path, output_path):
    converter = AddonConverter(addon_path)
    packon_path = converter.create_packon(output_path)
    logger.info('packon created: {}'.format(packon_path))
    return packon_path

def convert_addon_zip(addon_zip_path, output_path):
    zip_name = os.path.basename(addon_zip_path)
    addon_name = zip_name.split('.',1)[0]
    dist = None
    with tempfile.mkdtemp(prefix='basskick_addon_convert-'+zip_name) as tmp_dir:
        with zipfile.ZipFile(addon_zip_path, 'r') as zip_ref:
            zip_ref.extractall(tmp_dir)
            module_path = os.path.join(tmp_dir, addon_name+'.py')
            if os.path.exists(module_path):
                packon_path = convert_addon(module_path, output_path)
            else:
                package_path = os.path.join(tmp_dir, addon_name)
                if not os.path.isdir(package_path):
                    raise Exception(
                        f"Cannot find a module or package named {addon_name} "
                        f"after extracting {addon_zip_path} to {tmp_dir}."
                    )
                packon_path = convert_addon(package_path, output_path)
    return packon_path

def install_addon(addon_path, benv_path):
    """
    Converts the given addon to a packon and install it
    it the given benv_path.

    The addon_path may be a module.py file, a package dir,
    or an addon.zip file.
    """
    output_path = os.path.join(benv_path, 'Addons')
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    if addon_path.endswith('.zip'):
        packon_path = convert_addon_zip(addon_path, output_path)
    else:
        packon_path = convert_addon(addon_path, output_path)
    
    benv = Benv(benv_path)
    benv.run('pip', 'install', '-e', packon_path)
