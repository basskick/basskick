import os
import click
import pluggy

from ..api import session

import logging
logger = logging.getLogger(__name__)

#
#   PLUGIN API
#

cli_plugin_spec = pluggy.HookspecMarker("basskick.cli")
cli_plugin_impl = pluggy.HookimplMarker("basskick.cli")


class CliPluginSpecs(object):

    @cli_plugin_spec
    def define_commands(group: click.Group, in_benv: bool):
        """Let the plugin add commands to a group.

        :param click_group: the click.Group() to fill.
        :param in_benv: True if the CLI is run from a benv.
        :return: None
        """

#
#   CLICK UTILS
#

def get_cli_session(argv):
    if not argv:
        argv='.'

    if os.path.isfile(argv):
        return session.BasskickSession.from_file(argv)
    return session.BasskickSession.from_path(argv)

session_arg = click.option('-S', '--session', required=False)


#
#   CLI MAIN
#

@click.group()
def basskick():
    pass


class CLI(object):

    def __init__(self, hook, root_cmd_group):
        self.hook = hook
        self.root_cmd_group = root_cmd_group
        self.in_benv = session.BasskickSession.is_in_benv()
        logger.info(
            'Running CLI {} benv'.format(
                self.in_benv and 'in' or 'outside'
            )
        )

    def define_commands(self):
        results = self.hook.define_commands(
            group=self.root_cmd_group,
            in_benv=self.in_benv,
        )

    def run(self):
        self.root_cmd_group()

def main():
    pm = pluggy.PluginManager("basskick.cli")
    pm.add_hookspecs(CliPluginSpecs)
    pm.load_setuptools_entrypoints("basskick.cli")

    from ..lib.cli import benv
    from ..lib.cli import packon
    from ..lib.cli import session
    from ..lib.cli import cmds    
    pm.register(benv)
    pm.register(packon)
    pm.register(session)
    pm.register(cmds)

    cli = CLI(pm.hook, basskick)
    cli.define_commands()
    cli.run()
